from __future__ import print_function
import sys

BT = "|BT|"
ET = "|ET|"
BV = "|BV|"
EV = "|EV|"

def combine(parse_file, feat_file, num_parses, out_file):
    """
    Generates files suitable for input to SVM - tk-learn.

    Parameters
    ==========
    parse_file : str
        file that has the parse trees.
    feat_file : str
        file that has the features.
    num_parses : int
        the number of top sentence combinations to comsider
    out_file : str
        the file in which to store the features.

    Returns
    =======
    None
    """

    out = open(out_file, 'wb')

    step = num_parses
    trees = combine_trees(parse_file, step)
    vecs = combine_vecs(feat_file)
    j = 0
    for i in range(len(trees)):
        if i % step**2 == 0 and i != 0:
            j += 1
        target, vector = vecs[j]
        new_line = ' '.join([target, trees[i], vector])
        #new_line = ' '.join([target, vector])
        print(new_line, file=out)
    out.close()

def combine_trees(parse_file, step):
    trees = []
    pf = open(parse_file, 'rb')
    lines = pf.readlines()
    lines = [line.strip() for line in lines]
    for i in range(0, len(lines)-2*step + 1, 2*step):
        for k in range (0, step):
            for j in range(step, 2*step):
                tree = ' '.join([BT, lines[i+k], ET, BT, lines[i+j], ET])
                print('join '+ str(i+k)+' ' +str(i+j))
                trees.append(tree)
    return trees

def combine_vecs(feat_file):
    ff = open(feat_file, 'rb')
    lines = ff.readlines()
    lines = [line.strip() for line in lines]
    return  [line.split(' ', 1) for line in lines]

def main():
    parsefile = sys.argv[1]
    featfile = sys.argv[2]
    numparses = int(sys.argv[3])
    outfile = sys.argv[4]
    combine(parsefile, featfile, numparses, outfile)

if __name__ == '__main__':
    main()
