# extract training features
python takelab_simple_features.py ../data/train-2012/STS.input.MSRpar.txt ../data/train-2012/STS.gs.MSRpar.txt ../train_feat/MSRpar.txt

python takelab_simple_features.py ../data/train-2012/STS.input.MSRvid.txt ../data/train-2012/STS.gs.MSRvid.txt  ../train_feat/MSRvid.txt

python takelab_simple_features.py ../data/train-2012/STS.input.SMTeuroparl.txt ../data/train-2012/STS.gs.SMTeuroparl.txt  ../train_feat/SMTeuroparl.txt

# extract testing features
python takelab_simple_features.py ../data/test-2012/STS.input.MSRpar.txt ../data/test-2012/STS.gs.MSRpar.txt  ../test_feat/MSRpar.txt

python takelab_simple_features.py ../data/test-2012/STS.input.MSRvid.txt ../data/test-2012/STS.gs.MSRvid.txt  ../test_feat/MSRvid.txt

python takelab_simple_features.py ../data/test-2012/STS.input.SMTeuroparl.txt ../data/test-2012/STS.gs.SMTeuroparl.txt  ../test_feat/SMTeuroparl.txt

python takelab_simple_features.py ../data/test-2012/STS.input.surprise.OnWN.txt ../data/test-2012/STS.gs.surprise.OnWN.txt  ../test_feat/onwn.txt

python takelab_simple_features.py ../data/test-2012/STS.input.surprise.SMTnews.txt ../data/test-2012/STS.gs.surprise.SMTnews.txt  ../test_feat/smtnews.txt

