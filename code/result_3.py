from __future__ import print_function
import sys
import os
import numpy as np
"""
usage : python result_3.py <infile> <numparses> <outfile>
"""
def main():
    infile = sys.argv[1]
    step = int(sys.argv[2])
    outfile = sys.argv[3]
    numbers = open(infile, 'rb').readlines()
    numbers = [x.strip() for x in numbers]
    numbers = [float(x) for x in numbers]
    mean_list = []
    max_list = []
    min_list = []
    for i in range(0, len(numbers), step ** 2):
        mean_val = np.mean(numbers[i:i+step ** 2])
        max_val = max(numbers[i:i+step**2])
        min_val = min(numbers[i:i+step**2])
        mean_list.append(mean_val)
        max_list.append(max_val)
        min_list.append(min_val)
    outfile_mean = outfile + '.mean'
    outfile_max = outfile + '.max'
    outfile_min = outfile + '.min'
    f_mean = open(outfile_mean, 'wb')
    f_max = open(outfile_max, 'wb')
    f_min = open(outfile_min, 'wb')
    for i in range(len(mean_list)):
        print("%.3f" % mean_list[i], file=f_mean)
        print("%.3f" % max_list[i], file=f_max)
        print("%.3f" % min_list[i], file=f_min)
    f_mean.close()
    f_max.close()
    f_min.close()

if __name__ == '__main__':
    main()



