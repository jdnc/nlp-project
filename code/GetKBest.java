import java.util.Collection;
import java.util.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.StringReader;

import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.trees.*;
import edu.stanford.nlp.util.ScoredObject;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.parser.lexparser.LexicalizedParserQuery;
import edu.stanford.nlp.parser.lexparser.Options;

public class GetKBest{
	
	public static void runParser(String infile, String outfile, String k){
		try {
			File writeFile = new File(outfile);
			PrintWriter pw = new PrintWriter(writeFile);
			// load already trained parser from stanford
			LexicalizedParser lp = LexicalizedParser.loadModel("edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz");
			// read input file sentence by sentence and parse it
			int count = 0;
			TreePrint tp = new TreePrint("oneline");
			BufferedReader bReader = new BufferedReader(new FileReader(infile));
			String line;
			while((line = bReader.readLine()) != null) {
			  count++;
			  String lines[] = line.split("\t");
			  String line1 = lines[0];
			  String line2 = lines[1];
			  TokenizerFactory<CoreLabel> tokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(), "");
			  Tokenizer<CoreLabel> tok1 = tokenizerFactory.getTokenizer(new StringReader(line1));
			  Tokenizer<CoreLabel> tok2 = tokenizerFactory.getTokenizer(new StringReader(line2));
			  List<CoreLabel> rawWords1 = tok1.tokenize();
			  List<CoreLabel> rawWords2 = tok2.tokenize();
			  // for sent1
			  Tree parse = lp.apply(rawWords1);
			  LexicalizedParserQuery lpq = lp.lexicalizedParserQuery();
			  lpq.parse(rawWords1);
			  List<ScoredObject<Tree> > kTrees = lpq.getKBestPCFGParses(Integer.parseInt(k));
			  for(ScoredObject<Tree> t : kTrees){
			    //System.out.println("in for");
				  tp.printTree(t.object(), pw);;
			      }  
			  // for sent2 (lazy but quick)
			  Tree parse2 = lp.apply(rawWords2);
			  LexicalizedParserQuery lpq2 = lp.lexicalizedParserQuery();
			  lpq.parse(rawWords2);
			  List<ScoredObject<Tree> > kTrees2 = lpq.getKBestPCFGParses(Integer.parseInt(k));
			  for(ScoredObject<Tree> t : kTrees2){
			    //System.out.println("in for");
				  tp.printTree(t.object(), pw);;
			      }  
			  
			}
			pw.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	// <infile> <outfile> <k>
	public static void main(String[] args){
		String infile = args[0];
		String outfile = args[1];
	//	int k = Integer.parseInt(args[2]);
		String k = args[2];
		runParser(infile, outfile, k);
		return;
	}
	
}
