Code:

1. GetKBest.java : takes the input data as given by the STS competition, and saves the top -k parses in the output file. Please see inline the code for mre comments

Compile
=======
(Please set the correct paths for the required jars as per your system)
javac -classpath "stanford-parser.jar:stanford-parser-3.3.1-models.jar" GetKBest.java

Run
===
java -classpath "stanford-parser.jar:stanford-parser-3.3.1-models.jar:." GetKBest in.txt
out.tree 3


2. combine_feats.py: Combines the different features, labels and parse trees in a format expected by SVM-light

Usage : python combine_feats.py <parse_tree_file> <num_parses> <gold_standard_file> <output_file>


3. result_3.py: Analyze the mean/min/max for the top-k trees
Usage: python result_3 <svr_classifier_output> <num_parses> <output_file> 

----------------------
In addition the data folder has the data

we include the takelab system that we used for generating feature vectors

we also include SVM light package that we used for regression and tree kernels.